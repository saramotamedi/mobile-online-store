<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Nwidart\Modules\Facades\Module;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        foreach (Module::collections() as $module) {
            $this->call("Modules\\" . $module->getName() . "\\Database\\Seeders\\" . $module->getName() . "DatabaseSeeder");
        }
    }
}
