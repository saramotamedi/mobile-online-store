<?php

namespace Modules\Menu\Interfaces;

use Modules\Menu\Http\Requests\GetMenuRequest;
use Modules\Menu\Transformers\MenuResource;

interface MenuServiceInterface
{
    /**
     * Get the menu as a MenuResource instance.
     *
     * @param GetMenuRequest $request
     * @return MenuResource The MenuResource instance representing the menu.
     */
    public function getMenu(GetMenuRequest $request): MenuResource;
}
