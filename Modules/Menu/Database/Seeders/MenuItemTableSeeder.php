<?php

namespace Modules\Menu\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Menu\Entities\MenuItem;

class MenuItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        MenuItem::create([
            'menu_id' => 1,
            'parent_id' => null,
            'name' => 'خانه',
            'slug' => '/',
            'position' => 0
        ]);

        $menu  = MenuItem::create([
            'menu_id' => 1,
            'parent_id' => null,
            'name' => 'محصولات',
            'slug' => '/products',
            'position' => 0
        ]);

        $child1 = MenuItem::create([
            'menu_id' => 1,
            'parent_id' => $menu->id,
            'name' => 'دسته بندی اول',
            'slug' => '/first-level',
            'position' => 0
        ]);

        $child2 = MenuItem::create([
            'menu_id' => 1,
            'parent_id' => $child1->id,
            'name' => 'دسته بندی دوم',
            'slug' => '/second-level',
            'position' => 0
        ]);

        $child3 = MenuItem::create([
            'menu_id' => 1,
            'parent_id' => $child2->id,
            'name' => 'دسته بندی سوم',
            'slug' => 'third-level',
            'position' => 0
        ]);

        $child4 = MenuItem::create([
            'menu_id' => 1,
            'parent_id' => $child3->id,
            'name' => 'دسته بندی چهارم',
            'slug' => 'fourth-level',
            'position' => 0
        ]);

        $child5 = MenuItem::create([
            'menu_id' => 1,
            'parent_id' => $child4->id,
            'name' => 'دسته بندی پنجم',
            'slug' => 'fifth-level',
            'position' => 0
        ]);

        $child6 = MenuItem::create([
            'menu_id' => 1,
            'parent_id' => $child5->id,
            'name' => 'دسته بندی ششم',
            'slug' => 'sixth-level',
            'position' => 0
        ]);
    }
}
