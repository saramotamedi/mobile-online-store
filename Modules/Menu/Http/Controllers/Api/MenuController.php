<?php

namespace Modules\Menu\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Modules\Menu\Http\Requests\GetMenuRequest;
use Modules\Menu\Services\MenuService;
use Modules\Menu\Transformers\MenuResource;

class MenuController extends Controller
{
    private $menuService;

    public function __construct(MenuService $menuService)
    {
        $this->menuService = $menuService;
    }

    /**
     * Get the menu based on the provided GetMenuRequest.
     *
     * @param GetMenuRequest $request The request instance.
     * @return MenuResource The MenuResource instance representing the menu.
     */
    public function index(GetMenuRequest $request): MenuResource
    {
        return $this->menuService->getMenu($request);
    }
}
