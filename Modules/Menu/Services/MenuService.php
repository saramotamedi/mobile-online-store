<?php

namespace Modules\Menu\Services;

use Modules\Menu\Entities\Menu;
use Modules\Menu\Http\Requests\GetMenuRequest;
use Modules\Menu\Interfaces\MenuServiceInterface;
use Modules\Menu\Transformers\MenuResource;

class MenuService implements MenuServiceInterface
{
    public $arr = [];

    /**
     * Get the menu based on the provided GetMenuRequest.
     *
     * @param GetMenuRequest $request The request instance.
     * @return MenuResource The MenuResource instance representing the menu.
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException if the menu with the given name is not found.
     */
    public function getMenu(GetMenuRequest $request): MenuResource
    {
        $name = $request->getName();

        $menus = Menu::whereName($name)->firstOrFail()->children()->whereNull('parent_id')->get();

        foreach ($menus as $menu) {
            if (count($menu->children) > 0) {
                array_push($this->arr, $menu);
                $this->getChildren($menu->children);
            }
        }

        return (new MenuResource($menus));
    }

    protected function getChildren($menus): void
    {
        foreach ($menus as $menu) {
            if (count($menu->children) > 0) {
                array_push($this->arr, $menu);
                $this->getChildren($menu->children);
            }
        }
    }
}
