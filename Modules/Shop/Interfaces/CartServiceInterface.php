<?php

namespace Modules\Shop\Interfaces;

use Illuminate\Contracts\Auth\Authenticatable;

interface CartServiceInterface
{
    /**
     * Add a product variant to the user's cart.
     *
     * @param mixed $productVariant The identifier for the product variant.
     * @param int $quantity The quantity of the product variant to add (default is 1).
     * @return void
     */
    public function addProductToCart($productVariant, $quantity = 1): void;

    /**
     * Get the items in the user's cart.
     *
     * @return array The cart items. The structure of each item depends on the storage mechanism (Redis, session).
     */
    public function getCartItems(): array;

    /**
     * Handle the user login event.
     *
     * @param Authenticatable $user The authenticated user.
     * @return void
     */
    public function handleUserLogin(Authenticatable $user): void;

    /**
     * Transfer cart items from session to user's storage.
     *
     * @param int $userId
     * @param $cartItems
     * @return void
     */
    public function transferCartItemsToRedis($userId, $cartItems): void;
}
