<?php

namespace Modules\Shop\Interfaces;

use Modules\Shop\Entities\Order;

interface OrderServiceInterface
{
    /**
     * Create an order for a user based on the provided product data.
     *
     * @param array $productData An array containing product data, including product_variant_id and quantity.
     * @param int   $userId      The ID of the user for whom the order is created.
     *
     * @return Order The created order instance.
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException If a product variant is not found.
     */
    public function createOrder(array $productData, int $userId): Order;
}
