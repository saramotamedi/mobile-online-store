<?php

return [
    'slider' => [
        'title' => 'یک خرید کامل و جذاب!',
        'button' => 'خرید موبایل'
    ],
    'options' => [
        'free_delivery' => [
            'title' => 'تحویل رایگان',
            'description' => 'توضیحات تحویل رایگان',
        ],
        'quality_quarantee' => [
            'title' => 'گارانتی کیفیت',
            'description' => 'توضیحات گارانتی کیفیت',
        ],
        'daily_offers' => [
            'title' => 'آفرهای روزانه',
            'description' => 'توضیحات آفرهای روزانه',
        ],
        'secure_payment' => [
            'title' => 'پرداخت امن',
            'description' => 'توضیحات پرداخت امن',
        ],
    ],
    'first_section' => [
        'title' => 'جدید ترین محصولات',
    ],
    'quate' => [
        'title' => 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد،',
        'name' => 'سارا معتمدی',
    ],
    'newsletters' => [
        'title' => 'عضویت در خبرنامه',
        'button' => 'عضویت',
        'input' => 'ایمل خود را وارد کنید',
        'description' => "برای عضویت در خبرنامه ایمیل خود را وارد کنید."
    ],
    'social' => [
        'instagram' => [
            'title' => 'خرید از اینستاگرام',
            'link' => 'example.com'
        ]
    ]
];
