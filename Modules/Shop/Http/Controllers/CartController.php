<?php

namespace Modules\Shop\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Shop\Entities\ProductVariant;
use Modules\Shop\Http\Requests\CartStoreRequest;
use Modules\Shop\Services\CartService;

class CartController extends Controller
{
    protected $cartService;

    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    /**
     * Store a newly created resource in storage.
     * @param CartStoreRequest $request
     * @return JsonResponse
     */
    public function store(CartStoreRequest $request) :JsonResponse
    {
        $quantity = $request->getQuantity();
        $productId = $request->getProductId();
        $attributeValues = $request->getAttributeValues();

        $productVariant = ProductVariant::whereProductId($productId)
            ->whereAttributes($attributeValues)
            ->first();

        if (!$productVariant) {
            return response()->json(['error' => 'Product variant not found'], 404);
        }

        $this->cartService->addProductToCart($productVariant, $quantity);

        return response()->json(['message' => 'Product added to cart successfully']);
    }

    /**
     * Get the items in the user's cart.
     *
     * @return JsonResponse The JSON response containing cart items.
     */
    public function getCartItems(): JsonResponse
    {
        $cartItems = $this->cartService->getCartItems();

        return response()->json(['cart_items' => $cartItems]);
    }
}
