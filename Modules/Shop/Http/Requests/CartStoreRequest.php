<?php

namespace Modules\Shop\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CartStoreRequest extends FormRequest
{
    /**
     * Handle a failed validation attempt and throw an HTTP response exception.
     *
     * @param \Illuminate\Contracts\Validation\Validator $validator
     *
     * @throws \Illuminate\Http\Exceptions\HttpResponseException
     */
    protected function failedValidation(Validator $validator): void
    {
        throw new HttpResponseException(response()->json(['error' => $validator->errors()], 422));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'product_id' => 'required|exists:products,id',
            'attribute_value_id' => 'required',
            'quantity' => 'nullable',
        ];
    }

    /**
     * Get the quantity from the request.
     *
     * @return int The quantity. Default is 1 if not specified.
     */
    public function getQuantity(): int
    {
        return $this->input('quantity', 1);
    }

    /**
     * Get the product ID from the request.
     *
     * @return int The product ID.
     */
    public function getProductId(): int
    {
        return $this->input('product_id');
    }

    /**
     * Get the attribute values from the request.
     *
     * @return string The attribute values.
     */
    public function getAttributeValues(): string
    {
        return $this->input('attribute_value_id');
    }
}
