<?php

namespace Modules\Shop\Repositories;

use Modules\Shop\Entities\OrderItem;

class OrderItemRepository
{
    /**
     * Create a new order item.
     *
     * @param array $data The data for creating the order item.
     *
     * @return OrderItem The created order item instance.
     */
    public function create(array $data): OrderItem
    {
        return OrderItem::create($data);
    }
}
