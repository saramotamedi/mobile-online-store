<?php

namespace Modules\Shop\Repositories;

class CartItemRepository
{
    /**
     * Create or update a cart item.
     *
     * @param array $data
     * @param array $cart
     * @param mixed $cartItemId
     * @return array Updated cart
     */
    public function createOrUpdateCartItem(array $data, array $cart, $cartItemId): array
    {
        if (isset($cart[$cartItemId])) {
            $cart[$cartItemId]['quantity'] += $data['quantity'];
        } else {
            $cart[$cartItemId] = $data;
        }

        return $cart;
    }
}
