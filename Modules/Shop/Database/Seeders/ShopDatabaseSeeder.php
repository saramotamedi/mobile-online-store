<?php

namespace Modules\Shop\Database\Seeders;

use Illuminate\Database\Seeder;

class ShopDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(AttributeTableSeeder::class);
         $this->call(AttributeValueTableSeeder::class);
         $this->call(ProductTableSeeder::class);
         $this->call(ProductVariantTableSeeder::class);
    }
}
