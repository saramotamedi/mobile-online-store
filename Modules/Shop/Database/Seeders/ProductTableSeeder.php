<?php

namespace Modules\Shop\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Shop\Entities\Product;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name' => 'گوشی سامسونگ a51',
        ]);

        Product::create([
            'name' => 'گوشی سامسونگ a52',
        ]);

        Product::create([
            'name' => 'گوشی سامسونگ a53',
        ]);
    }
}
