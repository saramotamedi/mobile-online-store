<?php

namespace Modules\Shop\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Shop\Entities\Attribute;

class AttributeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Attribute::create([
           'name' => 'رنگ',
        ]);

        Attribute::create([
            'name' => 'رم',
        ]);

        Attribute::create([
            'name' => 'حافظه',
        ]);
    }
}
