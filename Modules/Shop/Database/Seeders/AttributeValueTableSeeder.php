<?php

namespace Modules\Shop\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Shop\Entities\AttributeValue;

class AttributeValueTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AttributeValue::create([
            'attribute_id' => 1,
            'value' => 'قرمز',
        ]);

        AttributeValue::create([
            'attribute_id' => 1,
            'value' => 'بنفش',
        ]);

        AttributeValue::create([
            'attribute_id' => 1,
            'value' => 'آبی',
        ]);

        AttributeValue::create([
            'attribute_id' => 2,
            'value' => '۸',
        ]);

        AttributeValue::create([
            'attribute_id' => 2,
            'value' => '۱۶',
        ]);

        AttributeValue::create([
            'attribute_id' => 2,
            'value' => '۳۲',
        ]);

        AttributeValue::create([
            'attribute_id' => 3,
            'value' => '۱۲۸ گیگا بایت',
        ]);

        AttributeValue::create([
            'attribute_id' => 3,
            'value' => '۵۱۲ گیگا بایت',
        ]);

        AttributeValue::create([
            'attribute_id' => 3,
            'value' => '۱ ترابایت',
        ]);
    }
}
