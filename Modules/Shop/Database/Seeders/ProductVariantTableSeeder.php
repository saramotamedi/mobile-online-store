<?php

namespace Modules\Shop\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Shop\Entities\ProductVariant;

class ProductVariantTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductVariant::create([
            'product_id' => 1,
            'attributes' => [1,4,7],
            'price' => 12500000,
            'stock' => 12,
        ]);

        ProductVariant::create([
            'product_id' => 1,
            'attributes' => [2,5,8],
            'price' => 12500000,
            'stock' => 12,
        ]);

        ProductVariant::create([
            'product_id' => 1,
            'attributes' => [3,6,8],
            'price' => 12500000,
            'stock' => 12,
        ]);
    }
}
