<?php

namespace Modules\Shop\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

class ProductVariant extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'product_id',
        'attributes',
        'price',
        'stock'
    ];

    protected $casts = [
        'attributes' => 'json'
    ];

    /**
     * Retrieve product variants with their associated attribute values.
     *
     * @param \Illuminate\Support\Collection $productVariants The collection of product variants.
     * @return Collection The collection of product variants with attribute values.
     */
    public static function withAttributeValues($productVariants): Collection
    {
        $productVariants->each(function ($variant) {
            $attributeValueIds = $variant->attributes;
            $attributeValues = AttributeValue::whereIn('id', $attributeValueIds)->get();
            $variant['attributes'] = $attributeValues;
            $variant->setRelation('attributes', $attributeValues);
        });

        return $productVariants;
    }

    /**
     * Decode the JSON when accessing the attributes column.
     *
     * @param mixed $value
     * @return string
     */
    public function getAttributesAttribute($value): string
    {
        return json_decode($value, true);
    }

    /**
     * Encode the JSON when setting the attributes column.
     *
     * @param mixed $value
     * @return void
     */
    public function setAttributesAttribute($value): void
    {
        $this->attributes['attributes'] = json_encode($value);
    }
}
