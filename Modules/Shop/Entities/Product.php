<?php

namespace Modules\Shop\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'description'
    ];

    /**
     * Get the variants related to the product.
     *
     * @return HasMany The relationship for the variants.
     */
    public function variants(): HasMany
    {
        return $this->hasMany(ProductVariant::class);
    }

    /**
     * Get the product to which the variant belongs.
     *
     * @return BelongsTo The relationship for the product.
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Get all product variants related to the product.
     *
     * @return HasMany The relationship for the product variants.
     */
    public function productVariants(): HasMany
    {
        return $this->hasMany(ProductVariant::class);
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot(): void
    {
        parent::boot();

        static::creating(function ($product) {
            $product->slug = $product->generateSlug($product->name);
        });
    }

    /**
     * Generate a slug for the given value.
     *
     * @param string $value The value for which to generate a slug.
     * @return string The generated slug.
     */
    public function generateSlug($value): string
    {
        $slug = Str::slug(preg_replace('/\s+/', '-', $value), '-');

        return $slug;
    }
}
