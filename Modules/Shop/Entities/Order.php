<?php

namespace Modules\Shop\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'total_amount',
        'status',
    ];

    /**
     * Get the order items associated with this order.
     *
     * @return HasMany The relationship for the order items.
     */
    public function orderItems(): hasMany
    {
        return $this->hasMany(OrderItem::class);
    }
}
