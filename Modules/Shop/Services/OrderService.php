<?php

namespace Modules\Shop\Services;

use Modules\Shop\Entities\Order;
use Modules\Shop\Entities\ProductVariant;
use Modules\Shop\Repositories\OrderItemRepository;

class OrderService
{
    private $orderItemRepository;

    public function __construct(OrderItemRepository $orderItemRepository)
    {
        $this->orderItemRepository = $orderItemRepository;
    }

    /**
     * Create an order for a user based on the provided product data.
     *
     * @param array $productData An array containing product data, including product_variant_id and quantity.
     * @param int $userId The ID of the user for whom the order is created.
     *
     * @return Order The created order instance.
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException If a product variant is not found.
     */
    public function createOrder(array $productData, int $userId): Order
    {
        $order = Order::create(['user_id' => $userId]);

        foreach ($productData as $item) {
            $productVariantId = $item['product_variant_id'];
            $quantity = $item['quantity'];

            $productVariant = ProductVariant::findOrFail($productVariantId);

            $data = [
                'order_id' => $order->id,
                'product_variant_id' => $productVariantId,
                'quantity' => $quantity,
                'price' => $productVariant->price,
            ];

            $this->orderItemRepository->create($data);
        }

        return $order;
    }
}
