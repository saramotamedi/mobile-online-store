<?php

namespace Modules\Shop\Services;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Modules\Shop\Interfaces\CartServiceInterface;
use Modules\Shop\Repositories\CartItemRepository;

class CartService implements CartServiceInterface
{
    protected $cartItemRepository;

    public function __construct(CartItemRepository $cartItemRepository)
    {
        $this->cartItemRepository = $cartItemRepository;
    }

    /**
     * Add a product variant to the user's cart.
     *
     * @param mixed $productVariant The identifier for the product variant.
     * @param int $quantity The quantity of the product variant to add (default is 1).
     * @return void
     */
    public function addProductToCart($productVariant, $quantity = 1): void
    {
        if (auth()->check()) {
            $userId = auth()->id();
            $this->addToRedisCart($userId, $productVariant, $quantity);
        } else {
            $this->addToSessionCart($productVariant, $quantity);
        }
    }

    /**
     * Get the items in the user's cart.
     *
     * @return array The cart items. The structure of each item depends on the storage mechanism (Redis, session).
     */
    public function getCartItems(): array
    {
        if (auth()->check()) {
            $userId = auth()->id();
            return $this->getRedisCartItems($userId);
        } else {
            return $this->getSessionCartItems();
        }
    }

    /**
     * Add a product to the user's Redis cart.
     *
     * @param int $userId The ID of the user.
     * @param $productVariant
     * @param int $quantity The quantity of the product to add.
     * @return void
     */
    private function addToRedisCart($userId, $productVariant, $quantity): void
    {
        $cartKey = $this->getRedisCartKey($userId);

        $cartItems = Redis::hgetall($cartKey);

        $cartItemId = $productVariant->id;

        $cartItemData = [
            'product_id' => $productVariant->product_id,
            'product_variant_id' => $productVariant->id,
            'quantity'   => $quantity,
            'attributes' => $productVariant->attributes,
            'price'      => $productVariant->price,
        ];

        $updatedCart = $this->cartItemRepository->createOrUpdateCartItem($cartItemData, $cartItems, $cartItemId);

        Redis::hmset($cartKey, $updatedCart);
    }

    /**
     * Add a product variant to the user's session cart.
     *
     * @param mixed $productVariant The identifier for the product variant (e.g., product ID).
     * @param int $quantity The quantity of the product variant to add.
     * @return void
     */
    private function addToSessionCart($productVariant, $quantity): void
    {
        $cart = $this->getCartItems();

        $cartItemId = $productVariant->id;

        $cartItemData = [
            'product_id' => $productVariant->product_id,
            'product_variant_id' => $productVariant->id,
            'quantity'   => $quantity,
            'attributes' => $productVariant->attributes,
            'price'      => $productVariant->price,
        ];

        $updatedCart = $this->cartItemRepository->createOrUpdateCartItem($cartItemData, $cart, $cartItemId);

        session()->put('cart', $updatedCart);
    }

    /**
     * Get the items from the user's Redis cart.
     *
     * @param int $userId The ID of the user.
     * @return array The cart items retrieved from Redis.
     */
    private function getRedisCartItems($userId): array
    {
        $cartKey = $this->getRedisCartKey($userId);

        $cartItems = Redis::hgetall($cartKey);

        return $cartItems;
    }

    /**
     * Get the items from the user's session cart.
     *
     * @return array The cart items retrieved from the session.
     */
    private function getSessionCartItems(): array
    {
        return session()->get('cart', []);
    }

    /**
     * Get the Redis key for the user's cart.
     *
     * @param int $userId The ID of the user.
     * @return string The Redis key for the user's cart.
     */
    private function getRedisCartKey($userId): string
    {
        return 'cart:' . $userId;
    }

    /**
     * Handle the user login event.
     *
     * @param Authenticatable $user The authenticated user.
     * @return void
     */
    public function handleUserLogin(Authenticatable $user): void
    {
        $cartItems = $this->retrieveCartItemsFromSession();

        if (!empty($cartItems)) {
            $this->transferCartItemsToRedis($user->id, $cartItems);

            $this->clearCartItemsFromSession();
        }
    }

    /**
     * Retrieve cart items from the session.
     *
     * @return array
     */
    private function retrieveCartItemsFromSession(): array
    {
        return Session::get('cart', []);
    }

    /**
     * Clear cart items from the session.
     *
     * @return void
     */
    private function clearCartItemsFromSession(): void
    {
        Session::forget('cart');
    }

    /**
     * Transfer cart items from session to user's storage.
     *
     * @param int $userId
     * @param $cartItems
     * @return void
     */
    public function transferCartItemsToRedis($userId, $cartItems): void
    {
        $cartKey = $this->getRedisCartKey($userId);

        $existingCartItems = Redis::hgetall($cartKey);

        $sessionCartItems = $this->getSessionCartItems();

        $mergedCartItems = array_merge($existingCartItems, $sessionCartItems);

        Redis::hmset($cartKey, $mergedCartItems);

        $this->clearCartItemsFromSession();
    }
}
