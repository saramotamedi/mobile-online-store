<?php

namespace Modules\Auth\Services;

use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Hash;
use Modules\Auth\Http\Requests\AuthLoginRequest;
use Modules\Auth\Http\Requests\AuthRegisterRequest;
use Modules\Auth\Interfaces\AuthServiceInterface;
use Modules\User\Entities\User;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthService implements AuthServiceInterface
{
    /**
     * Register a new user based on the provided registration request.
     *
     * @param AuthRegisterRequest $request
     * @return User
     */
    public function register(AuthRegisterRequest $request): User
    {
        $existUser = User::wherePhone($request->getPhone())->first();

        if ($existUser) {
            return response()->json(['error', 'auth.messages.user_exist'], 422);
        }

        $user = User::create(array_merge(
            $request->all(),
            ['password' => Hash::make($request->getPassword())]
        ));

        return $user;
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param AuthLoginRequest $request
     * @return RedirectResponse
     */
    public function login(AuthLoginRequest $request): RedirectResponse
    {
        $credentials = $request->only(['email', 'password']);

        if (!auth()->attempt($credentials)) {
            return response()->json(['error' => 'auth.messages.unauthorized'], 401);
        }

        $user = User::whereEmail($request->getEmail())->first();
        $token = JWTAuth::fromUser($user);
        auth()->login($user);

        $this->respondWithToken($token);

        $redirectUrl = $request->input('redirect');

        return redirect()->intended($redirectUrl);
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token): JsonResponse
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Carbon::now()->addMinutes(config('auth.expires_at') * 60)
        ]);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(): JsonResponse
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }
}
