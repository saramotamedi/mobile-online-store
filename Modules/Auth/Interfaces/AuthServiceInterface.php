<?php

namespace Modules\Auth\Interfaces;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Modules\Auth\Http\Requests\AuthLoginRequest;
use Modules\Auth\Http\Requests\AuthRegisterRequest;
use Modules\User\Entities\User;

Interface AuthServiceInterface
{
    /**
     * Register a new user based on the provided registration request.
     *
     * @param  AuthRegisterRequest  $request
     * @return User
     */
    public function  register(AuthRegisterRequest $request): User;

    /**
     * Attempt to log in the user based on the provided login request.
     *
     * @param AuthLoginRequest $request
     * @return RedirectResponse
     */
    public function  login(AuthLoginRequest $request): RedirectResponse;

    /**
     * Log out the authenticated user based on the provided request.
     *
     * @return JsonResponse
     */
    public function  logout(): JsonResponse;
}
