<?php

namespace Modules\Auth\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Controller;
use Modules\Auth\Http\Requests\AuthLoginRequest;
use Modules\Auth\Http\Requests\AuthRegisterRequest;
use Modules\Auth\Services\AuthService;
use Modules\User\Transformers\UserResource;

class AuthController extends Controller
{
    protected $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * Register a new user.
     *
     * @param AuthRegisterRequest $request
     * @return UserResource
     */
    public function register(AuthRegisterRequest $request): UserResource
    {
        $user = $this->authService->register($request);

        return new UserResource($user);
    }

    /**
     * Log in a user.
     *
     * @param AuthLoginRequest $request
     * @return RedirectResponse
     */
    public function login(AuthLoginRequest $request): RedirectResponse
    {
        return $this->authService->login($request);
    }
}
