@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 my-5">
            <div class="card my-5">
                <div class="card-body my-5">
                    <form method="post" action="{{ url( route('api.login')) }}">
                        <!-- Email input -->
                        <div class="form-outline mb-4">
                            <label class="form-label" for="form2Example1">{{ __('auth::login.email') }}</label>
                            <input type="email" name="email" id="form2Example1" class="form-control" />
                        </div>

                        <!-- Password input -->
                        <div class="form-outline mb-4">
                            <label class="form-label" for="form2Example2">{{ __('auth::login.password') }}</label>
                            <input type="password" name="password" id="form2Example2" class="form-control" />
                        </div>

                        <!-- Submit button -->
                        <button type="submit" class="btn btn-outline-success btn-block mb-4">{{ __('auth::login.button') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
