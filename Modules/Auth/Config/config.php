<?php

return [
    'name' => 'Auth',
    'messages' => [
        'unauthorized' => 'درخواست نامعتبر است.',
        'user_exist' => 'این حساب کاربری قبلا ثبت شده است.'
    ],
    'expires_at' => 50,
];
