<footer id="footer" class="overflow-hidden">
    <div class="container">
        <div class="row">
            <div class="footer-top-area">
                <div class="row d-flex flex-wrap justify-content-between">
                    <div class="col-lg-6 col-sm-6 pb-3">
                        <div class="footer-menu">
                            <h2>{{ __('layouts.menus.brand') }}</h2>
                            <p>{{ __('layouts.footer.description') }}</p>
                            <div class="social-links">
                                <ul class="d-flex list-unstyled">
                                    <li>
                                        <a href="#">
                                            <svg class="facebook">
                                                <use xlink:href="#facebook"/>
                                            </svg>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <svg class="instagram">
                                                <use xlink:href="#instagram"/>
                                            </svg>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <svg class="twitter">
                                                <use xlink:href="#twitter"/>
                                            </svg>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <svg class="linkedin">
                                                <use xlink:href="#linkedin"/>
                                            </svg>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <svg class="youtube">
                                                <use xlink:href="#youtube"/>
                                            </svg>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 pb-3">
                        <div class="footer-menu contact-item">
                            <h5 class="widget-title text-uppercase pb-2">{{ __('layouts.footer.contact_us.title') }}</h5>
                            <p><a href="mailto:">{{ __('layouts.footer.contact_us.email') }}</a></p>
                            <p><a href="">{{ __('layouts.footer.contact_us.phone') }}</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
</footer>
<div id="footer-bottom">
    <div class="container">
        <div class="row d-flex flex-wrap justify-content-between">
            <div class="col-md-4 col-sm-6">
                <div class="Shipping d-flex">
                    <p>We ship with:</p>
                    <div class="card-wrap ps-2">
                        <img src="{{ asset('images/dhl.png') }}" alt="visa">
                        <img src="{{ asset('images/shippingcard.png') }}" alt="mastercard">
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="payment-method d-flex">
                    <p>Payment options:</p>
                    <div class="card-wrap ps-2">
                        <img src="{{ asset('images/visa.jpg') }}" alt="visa">
                        <img src="{{ asset('images/mastercard.jpg') }}" alt="mastercard">
                        <img src="{{ asset('images/paypal.jpg') }}" alt="paypal">
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="copyright">
                    <p>© Copyright 2023 Design by sara motamedi
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
